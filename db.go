package main

import (
	"log"
	"net/http"
	"os"
	"time"

	mgo "gopkg.in/mgo.v2"
)

func newCollection(s *mgo.Session, db, cl string, ttl time.Duration) *mgo.Collection {
	c := s.DB(db).C(cl)

	index := mgo.Index{
		Key:         []string{"updated"},
		Unique:      true,
		DropDups:    true,
		Background:  true,
		Sparse:      true,
		ExpireAfter: ttl * time.Minute,
	}

	err := c.EnsureIndex(index)
	if err != nil {
		log.Printf("Recreate DB index for collection %s because configuration was changed...\n", cl)
		c.DropIndex("updated")

		err := c.EnsureIndex(index)
		if err != nil {
			log.Println(err)
			os.Exit(1)
		}
	}
	return c
}

func addRequestStatToDB(r *http.Request, duration int) {
	stat := Tstatistics{
		Date:     time.Now(),
		Endpoint: r.URL.Path,
		Request:  r.URL.RequestURI(),
		Duration: duration}

	if err := cStatistics.Insert(stat); err != nil {
		log.Println("Statistics | Failed to add statistics about the request to DB")
	}
}
