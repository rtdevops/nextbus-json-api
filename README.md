# nextbus-json-api
NextBus public XML feed wrapper with JSON output. Works only with _sf-muni_ agency.

## Quick start
Install docker and docker-compose.

```
$ git clone https://gitlab.com/rtdevops/nextbus-json-api.git
$ cd nextbus-json-api
$ sed -i.bak s/MONGODB_PASS=/MONGODB_PASS=mypassword/g .env
$ docker-compose up -d
```

The application will be ready to go in 3-5 minutes. The startup takes several minutes only in case if there is no valid cache in the DB.
In order to check the application's status run below request:
```
$ curl -X GET "http://[ip]:8080/api/v1"
```
Expected response:
> {"name":"NextBus RESTful API","api":"v1","author":"Roman Tishchenko / rt-devops@yandex.ru"}

## Available endpoints and parameters
* /routeList
* /routeConfig
  * r=[routeTag]
  * verbose
  * terse
* /predictions
  * stopId=[stopID]
  * routeTag=[routeTag]
  * r=[routeTag]
  * s=[stopTag]
  * useShortTitles=true
* /predictionsForMultiStops
  * stops=[routeTag]|[stopTag] // multiple parameters accepted e.x. stops=[routeTag1]|[stopTag1]&stops=[routeTag2]|[stopTag3]
  * useShortTitles=true
* /schedule
  * r=[routeTag]
* /messages
  * r=[routeTag] // multiple parameters accepted e.x. r=[routeTag1]&r=[routeTag2]
* /vehicleLocations
  * r=[routeTag]
  * t=[epoch time in msec]
* /routesNotRunning
  * t=[epoch time in msec]
* /statistics

## Endpoints description
Please use original [documentation](https://www.nextbus.com/xmlFeedDocs/NextBusXMLFeed.pdf) to get basic information about data structures and API features. This wrapper supports most of the features of the NextBus API.
In addition, two new endpoints were added.

* /routesNotRunning?t=[epoch time in msec]

Returns all routes that don't run in a specified hour. The submitted value of _t_ is automatically rounded to hour. For example, 6800000 (epoch time) = 1AM GMT, 3600000 (epoch time) = 1AM GMT.
Example output:
```
$ curl -X GET "http://[ip]:8080/api/v1/routesNotRunning?t=7200000"

[
	{
		"tag": "E",
		"title": "E-Embarcadero",
		"startTime": 31800000,
		"finishTime": 68760000
	},
	{
		"tag": "L",
		"title": "L-Taraval",
		"startTime": 15720000,
		"finishTime": 92700000
	},
    ...
	{
		"tag": "N",
		"title": "N-Judah",
		"startTime": 16680000,
		"finishTime": 93480000
	}
]
```

* /statistics

Returns several metrics of server usage:
  * number of requests per endpoint
  * list of slowest requests with duration in msec (top 10)

Example output:
```
$ curl -X GET "http://[ip]:8080/api/v1/statistics"
{
	"requestsCounter": {
		"/api/v1": 4,
		"/api/v1/routesNotRunning": 1,
		"/api/v1/vehicleLocations": 2
	},
	"slowestRequests": [
		{
			"request": "/api/v1/routesNotRunning?t=7200000",
			"duration": 363
		},
		{
			"request": "/api/v1/vehicleLocations?r=6&t=0",
			"duration": 331
		},
		{
			"request": "/api/v1/vehicleLocations?r=6&t=0",
			"duration": 1
		},
		{
			"request": "/api/v1?r=6&t=0",
			"duration": 0
		},
		{
			"request": "/api/v1",
			"duration": 0
		},
		{
			"request": "/api/v1",
			"duration": 0
		},
		{
			"request": "/api/v1",
			"duration": 0
		}
	]
}
```

Statistics keeps data for last 15 minutes.

## Quick notes about architecture and made assumptions
This web service consists of two docker containers: go binary itself and mongodb. To make it works relatively fast I made a decision to implement caching layer and mongodb is used for that purpose.
For each API endpoint I create a separate collection and use TTL indexes to specify the time to live for the data. These values are different for each collection. The exact numbers were chosen based on personal assumptions and probably should be adjusted after getting the statistics of production usage. Default TTL for collections is specified below:
```
RoutesListCacheTTL = 1440 // minutes
RouteConfigCacheTTL = 720
PredictionsCacheTTL = 15
PredictionsMultiCacheTTL = 15
ScheduleCacheTTL = 360
MessagesCacheTTL = 5
VehicleLocationsCacheTTL = 5
RoutesNotRunningCacheTTL = 360
StatisticsCacheTTL = 15
```

The statistics (/statistics endpoint) is kept in DB for only 15 minutes because I assume that it's enough interval for any monitoring system to scrape this data and store/aggregate it in any dedicated storage independently.

## TODO
There are a couple of things I'd like to rewrite/improve. Take a look at TODO file.