package main

import (
	"flag"
	"log"
	"net/http"
	"net/url"
	"time"

	mgo "gopkg.in/mgo.v2"
)

var nextbusAPIURL = "http://webservices.nextbus.com/service/publicXMLFeed"
var agency = "sf-muni"

var cRoutesList *mgo.Collection
var cRouteConfig *mgo.Collection
var cPredictions *mgo.Collection
var cPredictionsMulti *mgo.Collection
var cSchedule *mgo.Collection
var cMessages *mgo.Collection
var cVehicleLocations *mgo.Collection
var cRoutesNotRunning *mgo.Collection
var cStatistics *mgo.Collection

var cRoutesListCacheTTL time.Duration = 1440 // minutes
var cRouteConfigCacheTTL time.Duration = 720
var cPredictionsCacheTTL time.Duration = 15
var cPredictionsMultiCacheTTL time.Duration = 15
var cScheduleCacheTTL time.Duration = 360
var cMessagesCacheTTL time.Duration = 5
var cVehicleLocationsCacheTTL time.Duration = 5
var cRoutesNotRunningCacheTTL time.Duration = 360
var cStatisticsCacheTTL time.Duration = 15

func main() {
	cache := flag.Bool("c", true, "Disable filling up the cache at startup")
	mongouri := flag.String("m", "", "MongoDB connection string like: mongodb://<user>:<password>@<server>:<port>/<db>")
	flag.Parse()

	if *mongouri == "" {
		log.Println("Please specify mongo connection string with -m argument")
	}

	var session *mgo.Session
	var err error

	for {
		session, err = mgo.Dial(*mongouri)
		if err != nil {
			log.Println(err)
		}
		if session != nil {
			break
		}
	}
	session.SetMode(mgo.Monotonic, true)
	defer session.Close()

	cRoutesList = newCollection(session, "nextbus", "routesList", cRoutesListCacheTTL)
	cRouteConfig = newCollection(session, "nextbus", "routeConfig", cRouteConfigCacheTTL)
	cPredictions = newCollection(session, "nextbus", "predictions", cPredictionsCacheTTL)
	cPredictionsMulti = newCollection(session, "nextbus", "predictionsMulti", cPredictionsMultiCacheTTL)
	cSchedule = newCollection(session, "nextbus", "schedule", cScheduleCacheTTL)
	cMessages = newCollection(session, "nextbus", "messages", cMessagesCacheTTL)
	cVehicleLocations = newCollection(session, "nextbus", "vehicleLocations", cVehicleLocationsCacheTTL)
	cRoutesNotRunning = newCollection(session, "nextbus", "routesNotRunning", cRoutesNotRunningCacheTTL)
	cStatistics = newCollection(session, "nextbus", "statistics", cStatisticsCacheTTL)

	if *cache {
		log.Printf("Initializing (takes up to 5 mins)...\n\n")

		u, _ := url.Parse("?t=0")
		r := &http.Request{URL: u}
		getNotRunningRoutes(r, false)

		u, _ = url.Parse("")
		r = &http.Request{URL: u}
		getRouteConfig(r)

		log.Printf("\n\nInitializing was finished sucessfully. Waiting for requests...\n")
	}

	router := NewRouter()
	log.Fatal(http.ListenAndServe(":8080", router))
}
