package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"log"
)

func getVehicleLocations(r *http.Request) (TvehicleLocations, jsonError) {
	urlParams := r.URL.RequestURI()
	result := TvehicleLocationsEx{}

	// looking in cache first
	if err := cVehicleLocations.Find(bson.M{"request": urlParams}).One(&result); err == nil {
		log.Printf("VehicleLocations | Taking a record from cache for %s call...\n", urlParams)
		return result.VehicleLocations, nil
	}

	log.Printf("VehicleLocations | Cache doesn't exist for %s call. Calling NextBus API...\n", urlParams)

	vehicleLocations := &TvehicleLocations{}
	client := &http.Client{}

	req, err := http.NewRequest("GET", nextbusAPIURL, nil)
	if err != nil {
		log.Printf("VehicleLocations | Failed to create HTTP-request for %s call\n", urlParams)
		log.Println(err)
		return *vehicleLocations, raiseError("Failed to create HTTP-request for a call")

	}

	req.Header.Set("Accept-Encoding", "gzip, deflate")

	q := req.URL.Query()
	q.Add("command", "vehicleLocations")
	q.Add("a", agency)

	_ = r.ParseForm()
	if routeTag, ok := r.Form["r"]; ok {
		q.Add("r", routeTag[0])
	}
	if time, ok := r.Form["t"]; ok {
		q.Add("t", time[0])
	}
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("VehicleLocations | Failed to reach NextBus API for %s call\n", urlParams)
		log.Println(err)
		return *vehicleLocations, raiseError("Failed to reach NextBus API")
	}
	defer resp.Body.Close()

	respXML, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("VehicleLocations | Failed to parse body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *vehicleLocations, raiseError("Failed to find HTTP-body in the reply")
	}

	if err = gzxml2struct(respXML, vehicleLocations); err != nil {
		log.Printf("VehicleLocations | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *vehicleLocations, raiseError("Failed to parse XML from HTTP-response")
	}

	result = TvehicleLocationsEx{
		VehicleLocations: *vehicleLocations,
		Request:          r.URL.RequestURI(),
		Updated:          time.Now()}

	if len(result.VehicleLocations.Vehicle) == 0 {
		nextBusErr := &TnextBusError{}
		if err = gzxml2struct(respXML, nextBusErr); err != nil {
			log.Printf("VehicleLocations | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
			log.Println(err)
			return *vehicleLocations, raiseError("Failed to parse XML from HTTP-response")
		}
		if nextBusErr.Error.Text != "" {
			log.Println("VehicleLocations | ", strings.Trim(nextBusErr.Error.Text, "\n  "))
			return *vehicleLocations, raiseError(strings.Trim(nextBusErr.Error.Text, "\n  "))
		}
	}

	if err := cVehicleLocations.Insert(result); err != nil {
		log.Printf("VehicleLocations | Failed to cache the response for %s call. Database connection problem\n", urlParams)
		log.Println(err)
		return *vehicleLocations, raiseError("Failed to cache the response. Database connection problem")
	}

	return *vehicleLocations, nil
}

func VehicleLocationsHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	vehicleLocations := TvehicleLocations{}

	vehicleLocations, err := getVehicleLocations(r)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	result, err := struct2json(vehicleLocations)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	addRequestStatToDB(r, int(time.Since(start).Seconds()*1000))

	fmt.Fprint(w, result)
}
