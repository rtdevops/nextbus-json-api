package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var apiVersion = "/api/v1"

var routes = Routes{
	Route{
		"Index",
		"GET",
		apiVersion,
		Index,
	},
	Route{
		"RouteList",
		"GET",
		apiVersion + "/routeList",
		RoutesListHandler,
	},
	Route{
		"RouteConfig",
		"GET",
		apiVersion + "/routeConfig",
		RouteConfigHandler,
	},
	Route{
		"Predictions",
		"GET",
		apiVersion + "/predictions",
		PredictionsHandler,
	},
	Route{
		"PredictionsForMultiStops",
		"GET",
		apiVersion + "/predictionsForMultiStops",
		PredictionsForMultiStopsHandler,
	},
	Route{
		"Schedule",
		"GET",
		apiVersion + "/schedule",
		ScheduleHandler,
	},
	Route{
		"Messages",
		"GET",
		apiVersion + "/messages",
		MessagesHandler,
	},
	Route{
		"VehicleLocations",
		"GET",
		apiVersion + "/vehicleLocations",
		VehicleLocationsHandler,
	},
	Route{
		"RoutesNotRunning",
		"GET",
		apiVersion + "/routesNotRunning",
		RoutesNotRunningHandler,
	},
	Route{
		"Statistics",
		"GET",
		apiVersion + "/statistics",
		statisticsHandler,
	},
}
