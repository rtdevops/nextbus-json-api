package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"log"
)

func getSchedule(r *http.Request) (TscheduleEx, jsonError) {
	urlParams := r.URL.RequestURI()
	result := TscheduleEx{}

	// looking in cache first
	if err := cSchedule.Find(bson.M{"request": urlParams}).One(&result); err == nil {
		log.Printf("RouteSchedule | Taking a record from cache for %s call...\n", urlParams)
		return result, nil
	}

	log.Printf("RouteSchedule | Cache doesn't exist for %s call. Calling NextBus API...\n", urlParams)

	routeScheduleEx := &TscheduleEx{}
	routeSchedule := &Tschedule{}

	client := &http.Client{}

	req, err := http.NewRequest("GET", nextbusAPIURL, nil)
	if err != nil {
		log.Printf("RouteSchedule | Failed to create HTTP-request for %s call\n", urlParams)
		log.Println(err)
		return *routeScheduleEx, raiseError("Failed to create HTTP-request")
	}

	req.Header.Set("Accept-Encoding", "gzip, deflate")

	q := req.URL.Query()
	q.Add("command", "schedule")
	q.Add("a", agency)

	_ = r.ParseForm()
	if routeTag, ok := r.Form["r"]; ok {
		q.Add("r", routeTag[0])
	}

	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("RouteSchedule | Failed to reach NextBus API for %s call\n", urlParams)
		log.Println(err)
		return *routeScheduleEx, raiseError("Failed to reach NextBus API")
	}
	defer resp.Body.Close()

	respXML, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("RouteSchedule | Failed to parse body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *routeScheduleEx, raiseError("Failed to find HTTP-body in the reply")
	}

	if err = gzxml2struct(respXML, routeSchedule); err != nil {
		log.Printf("RouteSchedule | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *routeScheduleEx, raiseError("Failed to parse XML in HTTP-response")
	}

	if len(routeSchedule.Route) == 0 {
		nextBusErr := &TnextBusError{}
		if err = gzxml2struct(respXML, nextBusErr); err != nil {
			log.Printf("PredictRouteScheduleions | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
			log.Println(err)
			return *routeScheduleEx, raiseError("Failed to parse XML from HTTP-response")
		}
		if nextBusErr.Error.Text != "" {
			log.Println("RouteSchedule |", strings.Trim(nextBusErr.Error.Text, "\n  "))
			return *routeScheduleEx, raiseError(strings.Trim(nextBusErr.Error.Text, "\n  "))
		}
	}

	routeScheduleEx.Schedule = *routeSchedule
	routeScheduleEx.RouteTag = routeSchedule.Route[0].Tag
	routeScheduleEx.RouteTitle = routeSchedule.Route[0].Title

	// calc route start and finish time
	routeStopTimes := []int{}
	for i := range (*routeScheduleEx).Schedule.Route {
		for v := range (*routeScheduleEx).Schedule.Route[i].Tr {
			for k := range (*routeScheduleEx).Schedule.Route[i].Tr[v].Stop {
				if (*routeScheduleEx).Schedule.Route[i].Tr[v].Stop[k].EpochTime > 0 {
					routeStopTimes = append(routeStopTimes, routeScheduleEx.Schedule.Route[i].Tr[v].Stop[k].EpochTime)
				}
			}
		}
	}
	startTime, finishTime := getSLiceEdges(routeStopTimes)
	routeScheduleEx.StartTimeEpoch = startTime
	routeScheduleEx.FinishTimeEpoch = finishTime
	routeScheduleEx.Updated = time.Now()
	routeScheduleEx.Request = r.URL.RequestURI()

	if err := cSchedule.Insert(routeScheduleEx); err != nil {
		log.Printf("RouteSchedule | Failed to cache the response for %s call. Database connection problem\n", urlParams)
		log.Println(err)
		return *routeScheduleEx, raiseError("Failed to cache the response. Database connection problem")
	}

	return *routeScheduleEx, nil
}

func ScheduleHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	routeSchedule := TscheduleEx{}

	routeSchedule, err := getSchedule(r)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	result, err := struct2json(routeSchedule)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	addRequestStatToDB(r, int(time.Since(start).Seconds()*1000))

	fmt.Fprint(w, result)
}
