package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"log"
)

func getPredictionsForMultiStops(r *http.Request) (TpredictionsMulti, jsonError) {
	urlParams := r.URL.RequestURI()
	result := TpredictionsMultiEx{}

	// looking in cache first
	if err := cPredictionsMulti.Find(bson.M{"request": urlParams}).One(&result); err == nil {
		log.Printf("PredictionsMulti | Taking a record from cache for %s call...\n", urlParams)
		return result.Predictions, nil
	}

	log.Printf("PredictionsMulti | Cache doesn't exist for %s call. Calling NextBus API...\n", urlParams)

	predictionsMulti := &TpredictionsMulti{}
	client := &http.Client{}

	req, err := http.NewRequest("GET", nextbusAPIURL, nil)
	if err != nil {
		log.Printf("PredictionsMulti | Failed to create HTTP-request for %s call\n", urlParams)
		log.Println(err)
		return *predictionsMulti, raiseError("Failed to create HTTP-request for a call")
	}

	req.Header.Set("Accept-Encoding", "gzip, deflate")

	q := req.URL.Query()
	q.Add("command", "predictionsForMultiStops")
	q.Add("a", agency)

	_ = r.ParseForm()
	if stops, ok := r.Form["stops"]; ok {
		for i := range stops {
			q.Add("stops", stops[i])
		}
	}

	if useShortTitles, ok := r.Form["useShortTitles"]; ok {
		q.Add("useShortTitles", useShortTitles[0])
	}
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("PredictionsMulti | Failed to reach NextBus API for %s call\n", urlParams)
		log.Println(err)
		return *predictionsMulti, raiseError("Failed to reach NextBus API")
	}
	defer resp.Body.Close()

	respXML, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("PredictionsMulti | Failed to parse body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *predictionsMulti, raiseError("Failed to find HTTP-body in the reply")
	}

	if err = gzxml2struct(respXML, predictionsMulti); err != nil {
		log.Printf("PredictionsMulti | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *predictionsMulti, raiseError("Failed to parse XML from HTTP-response")
	}

	result = TpredictionsMultiEx{
		Predictions: *predictionsMulti,
		Request:     r.URL.RequestURI(),
		Updated:     time.Now()}

	if len(result.Predictions.Predictions) == 0 {
		nextBusErr := &TnextBusError{}
		if err = gzxml2struct(respXML, nextBusErr); err != nil {
			log.Printf("PredictionsMulti | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
			log.Println(err)
			return *predictionsMulti, raiseError("Failed to parse XML from HTTP-response")
		}
		if nextBusErr.Error.Text != "" {
			log.Println("PredictionsMulti |", strings.Trim(nextBusErr.Error.Text, "\n  "))
			return *predictionsMulti, raiseError(strings.Trim(nextBusErr.Error.Text, "\n  "))
		}
	}

	if err := cPredictionsMulti.Insert(result); err != nil {
		log.Printf("PredictionsMulti | Failed to cache the response for %s call. Database connection problem\n", urlParams)
		log.Println(err)
		return *predictionsMulti, raiseError("Failed to cache the response. Database connection problem")
	}

	return *predictionsMulti, nil
}

func PredictionsForMultiStopsHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	predictionsMulti := TpredictionsMulti{}

	predictionsMulti, err := getPredictionsForMultiStops(r)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	result, err := struct2json(predictionsMulti)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	addRequestStatToDB(r, int(time.Since(start).Seconds()*1000))

	fmt.Fprint(w, result)
}
