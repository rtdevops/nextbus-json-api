package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"log"
)

func getPredictions(r *http.Request) (Tpredictions, jsonError) {
	urlParams := r.URL.RequestURI()
	result := TpredictionsEx{}

	// looking in cache first
	if err := cPredictions.Find(bson.M{"request": urlParams}).One(&result); err == nil {
		log.Printf("Predictions | Taking a record from cache for %s call...\n", urlParams)
		return result.Predictions, nil
	}

	log.Printf("Predictions | Cache doesn't exist for %s call. Calling NextBus API...\n", urlParams)

	predictions := &Tpredictions{}
	client := &http.Client{}

	req, err := http.NewRequest("GET", nextbusAPIURL, nil)
	if err != nil {
		log.Printf("Predictions | Failed to create HTTP-request for %s call\n", urlParams)
		log.Println(err)
		return *predictions, raiseError("Failed to create HTTP-request")
	}

	req.Header.Set("Accept-Encoding", "gzip, deflate")

	q := req.URL.Query()
	q.Add("command", "predictions")
	q.Add("a", agency)

	_ = r.ParseForm()
	if stopID, ok := r.Form["stopId"]; ok {
		q.Add("stopId", stopID[0])
	}

	if routeTag, ok := r.Form["routeTag"]; ok {
		q.Add("routeTag", routeTag[0])
	}

	if routeTag, ok := r.Form["r"]; ok {
		q.Add("r", routeTag[0])
	}

	if stopTag, ok := r.Form["s"]; ok {
		q.Add("s", stopTag[0])
	}

	if useShortTitles, ok := r.Form["useShortTitles"]; ok {
		q.Add("useShortTitles", useShortTitles[0])
	}
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Predictions | Failed to reach NextBus API for %s call\n", urlParams)
		log.Println(err)
		return *predictions, raiseError("Failed to reach NextBus API")
	}
	defer resp.Body.Close()

	respXML, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Predictions | Failed to parse body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *predictions, raiseError("Failed to find HTTP-body in the reply")
	}

	if err = gzxml2struct(respXML, predictions); err != nil {
		log.Printf("Predictions | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *predictions, raiseError("Failed to parse XML from HTTP-response")
	}

	result = TpredictionsEx{
		Predictions: *predictions,
		Request:     r.URL.RequestURI(),
		Updated:     time.Now()}

	if len(result.Predictions.Predictions.Direction.Prediction) == 0 {
		nextBusErr := &TnextBusError{}
		if err = gzxml2struct(respXML, nextBusErr); err != nil {
			log.Printf("Predictions | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
			log.Println(err)
			return *predictions, raiseError("Failed to parse XML from HTTP-response")
		}
		if nextBusErr.Error.Text != "" {
			log.Println("Predictions |", strings.Trim(nextBusErr.Error.Text, "\n  "))
			return *predictions, raiseError(strings.Trim(nextBusErr.Error.Text, "\n  "))
		}
	}

	if err := cPredictions.Insert(result); err != nil {
		log.Printf("Predictions | Failed to cache the response for %s call. Database connection problem\n", urlParams)
		log.Println(err)
		return *predictions, raiseError("Failed to cache the response. Database connection problem")
	}

	return *predictions, nil
}

func PredictionsHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	predictions := Tpredictions{}

	predictions, err := getPredictions(r)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	result, err := struct2json(predictions)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	addRequestStatToDB(r, int(time.Since(start).Seconds()*1000))

	fmt.Fprint(w, result)
}
