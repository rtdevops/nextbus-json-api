package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"log"
)

func getMessages(r *http.Request) (Tmessages, jsonError) {
	urlParams := r.URL.RequestURI()
	result := TmessagesEx{}

	// looking in cache first
	if err := cMessages.Find(bson.M{"request": urlParams}).One(&result); err == nil {
		log.Printf("Messages | Taking a record from cache for %s call...\n", urlParams)
		return result.Message, nil
	}

	log.Printf("Messages | Cache doesn't exist for %s call. Calling NextBus API...\n", urlParams)

	messages := &Tmessages{}
	client := &http.Client{}

	req, err := http.NewRequest("GET", nextbusAPIURL, nil)
	if err != nil {
		log.Printf("Messages | Failed to create HTTP-request for %s call\n", urlParams)
		log.Println(err)
		return *messages, raiseError("Failed to create HTTP-request for a call")
	}

	req.Header.Set("Accept-Encoding", "gzip, deflate")

	q := req.URL.Query()
	q.Add("command", "messages")
	q.Add("a", agency)

	_ = r.ParseForm()
	if routeTags, ok := r.Form["r"]; ok {
		for i := range routeTags {
			q.Add("r", routeTags[i])
		}
	}
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Messages | Failed to reach NextBus API for %s call\n", urlParams)
		log.Println(err)
		return *messages, raiseError("Failed to reach NextBus API")
	}
	defer resp.Body.Close()

	respXML, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Messages | Failed to parse body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *messages, raiseError("Failed to find HTTP-body in the reply")
	}

	if err = gzxml2struct(respXML, messages); err != nil {
		log.Printf("Messages | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *messages, raiseError("Failed to parse XML from HTTP-response")
	}

	result = TmessagesEx{
		Message: *messages,
		Request: r.URL.RequestURI(),
		Updated: time.Now()}

	if len(result.Message.Route) == 0 {
		nextBusErr := &TnextBusError{}
		if err = gzxml2struct(respXML, nextBusErr); err != nil {
			log.Printf("Messages | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
			log.Println(err)
			return *messages, raiseError("Failed to parse XML from HTTP-response")
		}
		if nextBusErr.Error.Text != "" {
			log.Println("Messages |", strings.Trim(nextBusErr.Error.Text, "\n  "))
			return *messages, raiseError(strings.Trim(nextBusErr.Error.Text, "\n  "))
		}
	}

	if err := cMessages.Insert(result); err != nil {
		log.Printf("Messages | Failed to cache the response for %s call. Database connection problem\n", urlParams)
		log.Println(err)
		return *messages, raiseError("Failed to cache the response. Database connection problem")
	}

	return *messages, nil
}

func MessagesHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	messages := Tmessages{}

	messages, err := getMessages(r)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	result, err := struct2json(messages)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	addRequestStatToDB(r, int(time.Since(start).Seconds()*1000))

	fmt.Fprint(w, result)
}
