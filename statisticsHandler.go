package main

import (
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

func statisticsHandler(w http.ResponseWriter, r *http.Request) {
	// get list of top 10 slowest requests
	slRequests := []TstatSlowestRequests{}
	err := cStatistics.Find(bson.M{}).Select(bson.M{"request": 1, "duration": 1}).Sort("-duration").Limit(10).All(&slRequests)
	if err != nil {
		panic(err)
	}

	result := TstatisticsEx{
		SlowestRequests: slRequests,
		Counter:         map[string]int{}}

	// get count of requests per endpoint
	requestCount := []Tstatistics{}
	err = cStatistics.Find(bson.M{}).Select(bson.M{"endpoint": 1}).All(&requestCount)

	for i := range requestCount {
		result.Counter[requestCount[i].Endpoint]++

	}

	resultJSON, err := struct2json(result)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	fmt.Fprint(w, resultJSON)
}
