package main

import "time"

// Main
type Terror struct {
	Message string `json:"msg"`
}

type jsonError interface{}

type TnextBusError struct {
	Error struct {
		ShouldRetry string `xml:" shouldRetry,attr"  json:"shouldRetry"`
		Text        string `xml:",chardata" json:"text"`
	} `xml:" Error,omitempty" json:"error"`
	Copyright string `xml:" copyright,attr" json:"copyright"`
}

/* Handlers */

// Index
type Tindex struct {
	Name   string `json:"name"`
	API    string `json:"api"`
	Author string `json:"author"`
}

// RouteList
type TroutesList struct {
	Route []struct {
		Tag   string `xml:" tag,attr" json:"tag"`
		Title string `xml:" title,attr" json:"title"`
	} `xml:" route,omitempty" json:"route"`
	Copyright string `xml:" copyright,attr" json:"copyright"`
}

type TroutesListEx struct {
	RoutesList TroutesList `json:"routesList"`
	Request    string      `json:"request"`
	Updated    time.Time   `json:"updated"`
}

// RouteConfigIndex
type TrouteConfigIndex struct {
	Route []struct {
		Stop []struct {
			Tag    string `xml:" tag,attr" json:"tag"`
			Title  string `xml:" title,attr" json:"title"`
			Lat    string `xml:" lat,attr" json:"lat"`
			Lon    string `xml:" lon,attr" json:"lon"`
			StopID string `xml:" stopId,attr" json:"stopId"`
		} `xml:" stop,omitempty" json:"stop"`
		Direction []struct {
			Stop []struct {
				Tag string `xml:" tag,attr" json:"tag"`
			} `xml:" stop,omitempty" json:"stop"`
			Tag      string `xml:" tag,attr" json:"tag"`
			Title    string `xml:" title,attr" json:"title"`
			Name     string `xml:" name,attr" json:"name"`
			UseForUI string `xml:" useForUI,attr" json:"useForUI"`
		} `xml:" direction,omitempty" json:"direction"`
		Path []struct {
			Point []struct {
				Lat string `xml:" lat,attr" json:"lat"`
				Lon string `xml:" lon,attr" json:"lon"`
			} `xml:" point,omitempty" json:"point"`
		} `xml:" path,omitempty" json:"path"`
		Tag           string `xml:" tag,attr" json:"tag"`
		Title         string `xml:" title,attr" json:"title"`
		Color         string `xml:" color,attr" json:"color"`
		OppositeColor string `xml:" oppositeColor,attr" json:"oppositeColor"`
		LatMin        string `xml:" latMin,attr" json:"latMin"`
		LatMax        string `xml:" latMax,attr" json:"latMax"`
		LonMin        string `xml:" lonMin,attr" json:"lonMin"`
		LonMax        string `xml:" lonMax,attr" json:"lonMax"`
	} `xml:" route,omitempty" json:"route"`
	Copyright string `xml:" copyright,attr" json:"copyright"`
}

type TrouteConfigIndexEx struct {
	RouteConfig TrouteConfigIndex `json:"routeConfig"`
	Request     string            `json:"request"`
	Updated     time.Time         `json:"updated"`
}

// RouteConfig
type TrouteConfig struct {
	Route struct {
		Stop []struct {
			Tag    string `xml:" tag,attr" json:"tag"`
			Title  string `xml:" title,attr" json:"title"`
			Lat    string `xml:" lat,attr" json:"lat"`
			Lon    string `xml:" lon,attr" json:"lon"`
			StopID string `xml:" stopId,attr" json:"stopId"`
		} `xml:" stop,omitempty" json:"stop"`
		Direction []struct {
			Stop []struct {
				Tag string `xml:" tag,attr" json:"tag"`
			} `xml:" stop,omitempty" json:"stop"`
			Tag      string `xml:" tag,attr" json:"tag"`
			Title    string `xml:" title,attr" json:"title"`
			Name     string `xml:" name,attr" json:"name"`
			UseForUI string `xml:" useForUI,attr" json:"useForUI"`
		} `xml:" direction,omitempty" json:"direction"`
		Path []struct {
			Point []struct {
				Lat string `xml:" lat,attr" json:"lat"`
				Lon string `xml:" lon,attr" json:"lon"`
			} `xml:" point,omitempty" json:"point"`
			Tag []struct {
				ID string `xml:" id,attr"  json:"id"`
			} `xml:" tag,omitempty" json:"tag"`
		} `xml:" path,omitempty" json:"path"`
		Tag           string `xml:" tag,attr" json:"tag"`
		Title         string `xml:" title,attr" json:"title"`
		Color         string `xml:" color,attr" json:"color"`
		OppositeColor string `xml:" oppositeColor,attr" json:"oppositeColor"`
		LatMin        string `xml:" latMin,attr" json:"latMin"`
		LatMax        string `xml:" latMax,attr" json:"latMax"`
		LonMin        string `xml:" lonMin,attr" json:"lonMin"`
		LonMax        string `xml:" lonMax,attr" json:"lonMax"`
	} `xml:" route,omitempty" json:"route"`
	Copyright string `xml:" copyright,attr" json:"copyright"`
}

/*type Chipath struct {
	Chipoint []*Chipoint `xml:" point,omitempty" json:"point,omitempty"`
	Chitag   []*Chitag   `xml:" tag,omitempty" json:"tag,omitempty"`
}

type Chitag struct {
	Attr_id string `xml:" id,attr"  json:",omitempty"`
}
*/
type TrouteConfigEx struct {
	RouteConfig TrouteConfig `json:"routeConfig"`
	Request     string       `json:"request"`
	Updated     time.Time    `json:"updated"`
}

// Predictions
type Tpredictions struct {
	Predictions struct {
		Direction struct {
			Prediction []struct {
				EpochTime         string `xml:" epochTime,attr" json:"epochTime"`
				Seconds           string `xml:" seconds,attr" json:"seconds"`
				Minutes           string `xml:" minutes,attr" json:"minutes"`
				IsDeparture       string `xml:" isDeparture,attr" json:"isDeparture"`
				DirTag            string `xml:" dirTag,attr" json:"dirTag"`
				Vehicle           string `xml:" vehicle,attr" json:"vehicle"`
				Block             string `xml:" block,attr" json:"block"`
				TripTag           string `xml:" tripTag,attr" json:"tripTag"`
				AffectedByLayover string `xml:" affectedByLayover,attr" json:"affectedByLayover,omitempty"`
			} `xml:" prediction,omitempty" json:"prediction"`
			Title string `xml:" title,attr" json:"title"`
		} `xml:" direction,omitempty" json:"direction"`
		Message []struct {
			Text     string `xml:" text,attr" json:"text"`
			Priority string `xml:" priority,attr" json:"priority"`
		} `xml:" message,omitempty" json:"message"`
		AgencyTitle string `xml:" agencyTitle,attr" json:"agencyTitle"`
		RouteTitle  string `xml:" routeTitle,attr" json:"routeTitle"`
		RouteTag    string `xml:" routeTag,attr"json:"routeTag"`
		StopTitle   string `xml:" stopTitle,attr" json:"stopTitle"`
		StopTag     string `xml:" stopTag,attr" json:"stopTag"`
	} `xml:" predictions,omitempty" json:"predictions"`
	Copyright string `xml:" copyright,attr" json:"copyright"`
}

type TpredictionsEx struct {
	Predictions Tpredictions `json:"predictions"`
	Request     string       `json:"request"`
	Updated     time.Time    `json:"updated"`
}

// PredictionsForMultiStops
type TpredictionsMulti struct {
	Predictions []struct {
		Direction struct {
			Prediction []struct {
				EpochTime         string `xml:" epochTime,attr" json:"epochTime"`
				Seconds           string `xml:" seconds,attr" json:"seconds"`
				Minutes           string `xml:" minutes,attr" json:"minutes"`
				IsDeparture       string `xml:" isDeparture,attr" json:"isDeparture"`
				DirTag            string `xml:" dirTag,attr" json:"dirTag"`
				Vehicle           string `xml:" vehicle,attr" json:"vehicle"`
				Block             string `xml:" block,attr" json:"block"`
				TripTag           string `xml:" tripTag,attr" json:"tripTag"`
				AffectedByLayover string `xml:" affectedByLayover,attr" json:"affectedByLayover,omitempty"`
			} `xml:" prediction,omitempty" json:"prediction"`
			Title string `xml:" title,attr" json:"title"`
		} `xml:" direction,omitempty" json:"direction"`
		Message []struct {
			Text     string `xml:" text,attr" json:"text"`
			Priority string `xml:" priority,attr" json:"priority"`
		} `xml:" message,omitempty" json:"message"`
		AgencyTitle string `xml:" agencyTitle,attr" json:"agencyTitle"`
		RouteTitle  string `xml:" routeTitle,attr" json:"routeTitle"`
		RouteTag    string `xml:" routeTag,attr"json:"routeTag"`
		StopTitle   string `xml:" stopTitle,attr" json:"stopTitle"`
		StopTag     string `xml:" stopTag,attr" json:"stopTag"`
	} `xml:" predictions,omitempty" json:"predictions"`
	Copyright string `xml:" copyright,attr" json:"copyright"`
}

type TpredictionsMultiEx struct {
	Predictions TpredictionsMulti `json:"predictions"`
	Request     string            `json:"request"`
	Updated     time.Time         `json:"updated"`
}

// Schedule
type Tschedule struct {
	Route []struct {
		Header struct {
			Stop []struct {
				Tag  string `xml:" tag,attr" json:"tag"`
				Text string `xml:",chardata" json:"text"`
			} `xml:" stop,omitempty" json:"stop"`
		} `xml:" header,omitempty" json:"header"`
		Tr []struct {
			Stop []struct {
				Tag       string `xml:" tag,attr" json:"tag"`
				EpochTime int    `xml:" epochTime,attr" json:"epochTime"`
				Text      string `xml:",chardata" json:"text"`
			} `xml:" stop,omitempty" json:"stop"`
			BlockID string `xml:" blockID,attr" json:"blockID"`
		} `xml:" tr,omitempty" json:"tr"`
		Tag           string `xml:" tag,attr" json:"tag"`
		Title         string `xml:" title,attr" json:"title"`
		ScheduleClass string `xml:" scheduleClass,attr" json:"scheduleClass"`
		ServiceClass  string `xml:" serviceClass,attr" json:"serviceClass"`
		Direction     string `xml:" direction,attr" json:"direction"`
	} `xml:" route,omitempty" json:"route"`
	Copyright string `xml:" copyright,attr" json:"copyright"`
}

type TscheduleEx struct {
	Schedule        Tschedule `json:"schedule"`
	RouteTag        string    `json:"routeTag"`
	RouteTitle      string    `json:"routeTitle"`
	StartTimeEpoch  int       `json:"startTimeEpoch"`
	FinishTimeEpoch int       `json:"finishTimeEpoch"`
	Request         string    `json:"request"`
	Updated         time.Time `json:"updated"`
}

// Messages
type Tmessages struct {
	Route []struct {
		Message []struct {
			Text             string `xml:" text,omitempty" json:"text"`
			ID               string `xml:" id,attr" json:"id"`
			SendToBuses      string `xml:" sendToBuses,attr" json:"sendToBuses"`
			Priority         string `xml:" priority,attr" json:"priority"`
			StartBoundary    string `xml:" startBoundary,attr" json:"startBoundary,omitempty"`
			StartBoundaryStr string `xml:" startBoundaryStr,attr" json:"startBoundaryStr,omitempty"`
			EndBoundary      string `xml:" endBoundary,attr" json:"endBoundary,omitempty"`
			EndBoundaryStr   string `xml:" endBoundaryStr,attr" json:"endBoundaryStr,omitempty"`
		} `xml:" message,omitempty" json:"message"`
		Tag string `xml:" tag,attr" json:"tag"`
	} `xml:" route,omitempty" json:"route"`
	Copyright string `xml:" copyright,attr" json:"copyright"`
}

type TmessagesEx struct {
	Message Tmessages `json:"message"`
	Request string    `json:"request"`
	Updated time.Time `json:"updated"`
}

// VehicleLocations
type TvehicleLocations struct {
	Vehicle []struct {
		ID               string `xml:" id,attr" json:"id"`
		RouteTag         string `xml:" routeTag,attr" json:"routeTag,omitempty"`
		DirTag           string `xml:" dirTag,attr" json:"dirTag,omitempty"`
		Lat              string `xml:" lat,attr" json:"lat"`
		Lon              string `xml:" lon,attr" json:"lon"`
		SecsSinceReport  string `xml:" secsSinceReport,attr" json:"secsSinceReport"`
		Predictable      string `xml:" predictable,attr" json:"predictable"`
		Heading          string `xml:" heading,attr" json:"heading"`
		SpeedKmHr        string `xml:" speedKmHr,attr" json:"speedKmHr"`
		LeadingVehicleID string `xml:" leadingVehicleId,attr" json:"leadingVehicleId,omitempty"`
	} `xml:" vehicle,omitempty" json:"vehicle"`
	LastTime struct {
		Time string `xml:" time,attr" json:"time"`
	} `xml:" lastTime,omitempty" json:"lastTime"`
	Copyright string `xml:" copyright,attr" json:"copyright"`
}

type TvehicleLocationsEx struct {
	VehicleLocations TvehicleLocations `json:"vehicleLocations"`
	Request          string            `json:"request"`
	Updated          time.Time         `json:"updated"`
}

// Not running routes
type TroutesNotRunning struct {
	RouteTag        string `json:"tag"`
	RouteTitle      string `json:"title"`
	StartTimeEpoch  int    `json:"startTime"`
	FinishTimeEpoch int    `json:"finishTime"`
}

type TroutesNotRunningEx struct {
	Routes             []TroutesNotRunning `json:"routes"`
	RequestedTimeEpoch int                 `json:"requestedTimeEpoch"`
	Updated            time.Time           `json:"updated"`
}

// Statistics
type Tstatistics struct {
	Date     time.Time `json:"date"`
	Endpoint string    `json:"endpoint"`
	Request  string    `json:"request"`
	Duration int       `json:"duration"`
}

type TstatSlowestRequests struct {
	Request  string `json:"request"`
	Duration int    `json:"duration"`
}

type TstatisticsEx struct {
	Counter         map[string]int         `json:"requestsCounter"`
	SlowestRequests []TstatSlowestRequests `json:"slowestRequests"`
}
