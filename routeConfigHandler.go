package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"log"
)

func getRouteConfig(r *http.Request) (TrouteConfig, jsonError) {
	urlParams := r.URL.RequestURI()
	result := TrouteConfigEx{}

	// looking in cache first
	if err := cRouteConfig.Find(bson.M{"request": urlParams}).One(&result); err == nil {
		log.Printf("RouteConfig | Taking a record from cache for %s call...\n", urlParams)
		return result.RouteConfig, nil
	}

	log.Printf("RouteConfig | Cache doesn't exist for %s call. Calling NextBus API...\n", urlParams)

	routeConfig := &TrouteConfig{}
	client := &http.Client{}

	req, err := http.NewRequest("GET", nextbusAPIURL, nil)
	if err != nil {
		log.Printf("RouteConfig | Failed to create HTTP-request for %s call\n", urlParams)
		log.Println(err)
		return *routeConfig, raiseError("Failed to create HTTP-request for a call")
	}

	req.Header.Set("Accept-Encoding", "gzip, deflate")

	q := req.URL.Query()
	q.Add("command", "routeConfig")
	q.Add("a", agency)

	_ = r.ParseForm()
	if _, ok := r.Form["verbose"]; ok {
		q.Add("verbose", "")
	}

	if _, ok := r.Form["terse"]; ok {
		q.Add("terse", "")
	}

	if routeTag, ok := r.Form["r"]; ok {
		q.Add("r", routeTag[0])
	}

	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("RouteConfig | Failed to reach NextBus API for %s call\n", urlParams)
		log.Println(err)
		return *routeConfig, raiseError("Failed to reach NextBus API")
	}
	defer resp.Body.Close()

	respXML, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("RouteConfig | Failed to parse body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *routeConfig, raiseError("Failed to find HTTP-body in the reply")
	}

	if err = gzxml2struct(respXML, routeConfig); err != nil {
		log.Printf("RouteConfig | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *routeConfig, raiseError("Failed to parse XML from HTTP-response")
	}

	result = TrouteConfigEx{
		RouteConfig: *routeConfig,
		Request:     r.URL.RequestURI(),
		Updated:     time.Now()}

	if len(result.RouteConfig.Route.Stop) == 0 {
		nextBusErr := &TnextBusError{}
		if err = gzxml2struct(respXML, nextBusErr); err != nil {
			log.Printf("RouteConfig | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
			log.Println(err)
			return *routeConfig, raiseError("Failed to parse XML from HTTP-response")
		}
		if nextBusErr.Error.Text != "" {
			log.Println("RouteConfig |", strings.Trim(nextBusErr.Error.Text, "\n  "))
			return *routeConfig, raiseError(strings.Trim(nextBusErr.Error.Text, "\n  "))
		}
	}

	if err := cRouteConfig.Insert(result); err != nil {
		log.Printf("RouteConfig | Failed to cache the response for %s call. Database connection problem\n", urlParams)
		log.Println(err)
		return *routeConfig, raiseError("Failed to cache the response. Database connection problem")
	}

	return *routeConfig, nil
}

func RouteConfigHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	routeConfig := TrouteConfig{}

	routeConfig, err := getRouteConfig(r)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	result, err := struct2json(routeConfig)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	addRequestStatToDB(r, int(time.Since(start).Seconds()*1000))

	fmt.Fprint(w, result)
}
