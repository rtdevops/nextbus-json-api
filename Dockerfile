FROM centos:7
MAINTAINER Roman Tishchenko / rt-devops@yandex.ru

RUN yum clean all \
    && yum -y update

COPY ./bin/nextbus /
COPY ./entrypoint.sh /

RUN chmod +x /nextbus

ENTRYPOINT ["/entrypoint.sh"]