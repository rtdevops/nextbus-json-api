package main

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"time"
)

func gunzip(gz []byte) (*gzip.Reader, error) {
	buf := bytes.NewBuffer(gz)
	reader, err := gzip.NewReader(buf)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return reader, nil
}

func gzxml2struct(xmlStr []byte, data interface{}) error {
	reader, err := gunzip(xmlStr)
	if err != nil {
		log.Println("Failed to gunzip HTTP-response")
		return err
	}

	dec := xml.NewDecoder(reader)
	err = dec.Decode(&data)
	if err != nil && err != io.EOF {
		log.Println("Failed to unmarshal XML")
		return err
	}
	return nil
}

func struct2json(data interface{}) (string, error) {
	result, err := json.Marshal(data)
	if err != nil {
		log.Println("Error marshalling to JSON")
		return "", err
	}

	return string(result), nil
}

func epochToHumanReadable(epoch int) string {
	return time.Unix(int64(epoch/1000), 0).Format("15:04PM (MST)")
}

func getSLiceEdges(d []int) (int, int) {
	smallest, biggest := d[0], d[0]
	for _, v := range d {
		if v > biggest {
			biggest = v
		}
		if v < smallest {
			smallest = v
		}
	}
	return smallest, biggest
}

func raiseError(msg string) jsonError {
	m := Terror{Message: msg}
	result, _ := struct2json(m)
	return result
}

func timeDifference(a, b time.Time) float64 {
	delta := a.Sub(b)
	fmt.Println(delta.Minutes())
	return delta.Minutes()
}

func roundtimeStampToHours(time int) int {
	return (time/1000 - (time / 1000 % 3600)) * 1000
}
