package main

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"gopkg.in/mgo.v2/bson"

	"log"
)

func getNotRunningRoutes(r *http.Request, usecache bool) ([]TroutesNotRunning, jsonError) {
	urlParams := r.URL.RequestURI()

	routesNotRunningEx := TroutesNotRunningEx{}
	var t int

	_ = r.ParseForm()
	if tmp, ok := r.Form["t"]; ok {
		time, err := strconv.Atoi(tmp[0])
		if err != nil {
			log.Println("RoutesNotRunning | ", err)
			return routesNotRunningEx.Routes, raiseError("Failed to parse %t argument")
		}
		t = time
	}

	rtime := roundtimeStampToHours(t)

	u, _ := url.Parse("")
	ra := &http.Request{URL: u}
	routesList, err := getRoutesList(ra)
	if err != nil {
		log.Println("RoutesNotRunning | ", err)
		return routesNotRunningEx.Routes, raiseError("Failed to get routes list from NextBus API")
	}

	if usecache {
		if err := cRoutesNotRunning.Find(bson.M{"requestedtimeepoch": rtime}).One(&routesNotRunningEx); err == nil {
			log.Printf("RoutesNotRunning | Taking a record from cache for %s call...\n", urlParams)
			return routesNotRunningEx.Routes, nil
		}
	}

	log.Printf("RoutesNotRunning | Cache doesn't exist for %s call. Calling NextBus API...\n", urlParams)

	routesScheduleEx := []TscheduleEx{}
	if err := cSchedule.Find(bson.M{}).All(&routesScheduleEx); err == nil {
		log.Println("RoutesNotRunning | Getting schedule for routes from cache...")
	}

	for v := range routesList.Route {
		position := -1
		log.Println("-------------------------------------------------------------------------------")
		for i := range routesScheduleEx {
			if routesList.Route[v].Tag == routesScheduleEx[i].RouteTag {
				position = i
			}
		}

		if position >= 0 {
			log.Printf("RoutesNotRunning | Schedule for route %s / %s exists in cache\n", routesList.Route[v].Tag, routesList.Route[v].Title)
			if rtime < roundtimeStampToHours(routesScheduleEx[position].StartTimeEpoch) && rtime > roundtimeStampToHours(routesScheduleEx[position].FinishTimeEpoch)-86400000 {
				log.Printf("RoutesNotRunning | Route %s / %s usually starts at %d / %s and finish at %d / %s. This route isn't available at requested time %d / %s. Adding to response...\n", routesList.Route[v].Tag, routesList.Route[v].Title, routesScheduleEx[position].StartTimeEpoch, epochToHumanReadable(routesScheduleEx[position].StartTimeEpoch), routesScheduleEx[position].FinishTimeEpoch, epochToHumanReadable(routesScheduleEx[position].FinishTimeEpoch), rtime, epochToHumanReadable(rtime))
				tmp := TroutesNotRunning{
					RouteTag:        routesList.Route[v].Tag,
					RouteTitle:      routesList.Route[v].Title,
					StartTimeEpoch:  routesScheduleEx[position].StartTimeEpoch,
					FinishTimeEpoch: routesScheduleEx[position].FinishTimeEpoch}

				routesNotRunningEx.Routes = append(routesNotRunningEx.Routes, tmp)
			} else {
				log.Printf("RoutesNotRunning | Route %s / %s usually starts at %d / %s and finish at %d / %s. This route is available at requested time %d / %s. Ignoring...\n", routesList.Route[v].Tag, routesList.Route[v].Title, routesScheduleEx[position].StartTimeEpoch, epochToHumanReadable(routesScheduleEx[position].StartTimeEpoch), routesScheduleEx[position].FinishTimeEpoch, epochToHumanReadable(routesScheduleEx[position].FinishTimeEpoch), rtime, epochToHumanReadable(rtime))
			}
		} else {
			log.Printf("RoutesNotRunning | Schedule for route %s / %s doesn't exist in cache\n", routesList.Route[v].Tag, routesList.Route[v].Title)
			u, _ := url.Parse(fmt.Sprint("?r=", routesList.Route[v].Tag))
			rn := &http.Request{URL: u}
			dbSchedule, err := getSchedule(rn)
			time.Sleep(1 * time.Second)
			if err != nil {
				log.Println("RoutesNotRunning | ", err)
				return routesNotRunningEx.Routes, raiseError("Failed to get schedule for route from NextBus API")
			}

			if rtime < roundtimeStampToHours(dbSchedule.StartTimeEpoch) && rtime > roundtimeStampToHours(dbSchedule.FinishTimeEpoch)-86400000 {
				log.Printf("RoutesNotRunning | Route %s / %s usually starts at %d / %s and finish at %d / %s. This route isn't available at requested time %d / %s. Adding to response...\n", routesList.Route[v].Tag, routesList.Route[v].Title, dbSchedule.StartTimeEpoch, epochToHumanReadable(dbSchedule.StartTimeEpoch), dbSchedule.FinishTimeEpoch, epochToHumanReadable(dbSchedule.FinishTimeEpoch), rtime, epochToHumanReadable(rtime))
				tmp := TroutesNotRunning{
					RouteTag:        routesList.Route[v].Tag,
					RouteTitle:      routesList.Route[v].Title,
					StartTimeEpoch:  dbSchedule.StartTimeEpoch,
					FinishTimeEpoch: dbSchedule.FinishTimeEpoch}

				routesNotRunningEx.Routes = append(routesNotRunningEx.Routes, tmp)
			} else {
				log.Printf("RoutesNotRunning | Route %s / %s usually starts at %d / %s and finish at %d / %s. This route is available at requested time %d / %s. Ignoring...\n", routesList.Route[v].Tag, routesList.Route[v].Title, dbSchedule.StartTimeEpoch, epochToHumanReadable(dbSchedule.StartTimeEpoch), dbSchedule.FinishTimeEpoch, epochToHumanReadable(dbSchedule.FinishTimeEpoch), rtime, epochToHumanReadable(rtime))
			}
		}
	}

	routesNotRunningEx.RequestedTimeEpoch = rtime
	routesNotRunningEx.Updated = time.Now()

	if err := cRoutesNotRunning.Insert(routesNotRunningEx); err != nil {
		log.Println("RoutesNotRunning | ", err)
		return routesNotRunningEx.Routes, raiseError("Failed to cache the response. Database connection problem")
	}
	return routesNotRunningEx.Routes, nil
}

func RoutesNotRunningHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	routesNotRunning := []TroutesNotRunning{}

	routesNotRunning, err := getNotRunningRoutes(r, true)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	result, err := struct2json(routesNotRunning)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	addRequestStatToDB(r, int(time.Since(start).Seconds()*1000))

	fmt.Fprint(w, result)
}
