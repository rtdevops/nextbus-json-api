package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"log"
)

func getRoutesList(r *http.Request) (TroutesList, jsonError) {
	urlParams := r.URL.RequestURI()
	result := TroutesListEx{}

	// looking in cache first
	if err := cRoutesList.Find(bson.M{"request": urlParams}).One(&result); err == nil {
		log.Printf("RoutesList | Taking a record from cache for %s call...\n", urlParams)
		return result.RoutesList, nil
	}

	log.Printf("RoutesList | Cache doesn't exist for %s call. Calling NextBus API...\n", urlParams)

	routesList := &TroutesList{}
	client := &http.Client{}

	req, err := http.NewRequest("GET", nextbusAPIURL, nil)
	if err != nil {
		log.Printf("RoutesList | Failed to create HTTP-request for %s call\n", urlParams)
		log.Println(err)
		return *routesList, raiseError("Failed to create HTTP-request for a call")
	}

	req.Header.Set("Accept-Encoding", "gzip, deflate")

	q := req.URL.Query()
	q.Add("command", "routeList")
	q.Add("a", agency)
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("RoutesList | Failed to reach NextBus API for %s call\n", urlParams)
		log.Println(err)
		return *routesList, raiseError("Failed to reach NextBus API")
	}
	defer resp.Body.Close()

	respXML, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("RoutesList | Failed to parse body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *routesList, raiseError("Failed to find HTTP-body in the reply")
	}

	if err := gzxml2struct(respXML, routesList); err != nil {
		log.Printf("RoutesList | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
		log.Println(err)
		return *routesList, raiseError("Failed to parse XML from HTTP-response")
	}

	result = TroutesListEx{
		RoutesList: *routesList,
		Request:    urlParams,
		Updated:    time.Now()}

	if len(result.RoutesList.Route) == 0 {
		nextBusErr := &TnextBusError{}
		if err = gzxml2struct(respXML, nextBusErr); err != nil {
			log.Printf("RoutesList | Failed to parse XML-body of HTTP-response for %s call\n", urlParams)
			log.Println(err)
			return *routesList, raiseError("Failed to parse XML from HTTP-response")
		}
		if nextBusErr.Error.Text != "" {
			log.Println("RoutesList |", strings.Trim(nextBusErr.Error.Text, "\n  "))
			return *routesList, raiseError(strings.Trim(nextBusErr.Error.Text, "\n  "))
		}
	}

	if err := cRoutesList.Insert(result); err != nil {
		log.Printf("RoutesList | Failed to cache the response for %s call. Database connection problem\n", urlParams)
		log.Println(err)
		return *routesList, raiseError("Failed to cache the response. Database connection problem")
	}
	return *routesList, nil
}

func RoutesListHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	routesList := TroutesList{}

	routesList, err := getRoutesList(r)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	result, err := struct2json(routesList)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	addRequestStatToDB(r, int(time.Since(start).Seconds()*1000))

	fmt.Fprint(w, result)
}
