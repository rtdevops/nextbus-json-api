package main

import (
	"fmt"
	"net/http"
	"time"

	"log"
)

func Index(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	data := Tindex{
		Name:   "NextBus RESTful API",
		API:    "v1",
		Author: "Roman Tishchenko / rt-devops@yandex.ru"}

	json, err := struct2json(data)
	if err != nil {
		log.Println("Index | Unexpected API error")
		return
	}

	addRequestStatToDB(r, int(time.Since(start).Seconds()*1000))

	fmt.Fprint(w, json)
}
